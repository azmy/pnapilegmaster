package com.bungker.com.pna.entities;

import android.arch.persistence.room.Embedded;
import android.arch.persistence.room.Relation;

import java.util.List;

public class PartaiWithCaleg {

    @Embedded
    public Partai partai;

    @Relation(parentColumn = "id", entityColumn = "partai_id", entity = Caleg.class)
    public List<Caleg> calegs;
}
