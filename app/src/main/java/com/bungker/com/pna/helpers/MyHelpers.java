package com.bungker.com.pna.helpers;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ExecutionException;

public class MyHelpers {

    private static String TAG = "MyHelpders";

    public static Date string2Date(String tgl, String format_awal, String format_akhir)  {
        Date date=null;
        try {
            SimpleDateFormat oldFormat = new SimpleDateFormat(format_awal);
            date = oldFormat.parse(tgl);
            SimpleDateFormat newFormat = new SimpleDateFormat(format_akhir);
            String sNewFormat = newFormat.format(date);
            return newFormat.parse(sNewFormat);
        }
        catch (ParseException pe){
            Log.e("Error", pe.getMessage());
           return new Date();
        }
    }

    public static String date2String(Date dt, String format_awal, String format_akhir){
        String sDate=null;
        try{
            if(dt!=null) {
                SimpleDateFormat oldFormat = new SimpleDateFormat(format_awal);
                sDate = oldFormat.format(dt);
            }
            return sDate;
        }
        catch(Exception e){
            Log.e("Error ", e.getMessage());
            return null;
        }
    }

    public static Drawable createToDrawable(Context context, String str){

        try{
            InputStream is = context.getAssets().open(str);
            Drawable drawable = Drawable.createFromStream(is,null);
            return drawable;
        }
        catch (IOException ie){
            return null;
        }
    }

    public static Bitmap createToBitmap(Context context, String str)
    {
        try {
            InputStream is = context.getAssets().open(str);
            Bitmap bitmap = BitmapFactory.decodeStream(is);
            return bitmap;
        }
        catch (IOException ie){
            return null;
        }
    }

    public static Bitmap bitmapFromUrl(final String string){
        try {
            return new AsyncTask<String, Void, Bitmap>() {
                @Override
                protected Bitmap doInBackground(String... strings) {
                    Bitmap bitmap;
                    try {
                        InputStream is = new URL( strings[0] ).openStream();
                        bitmap= BitmapFactory.decodeStream( is );
                    }
                    catch (MalformedURLException mue){
                        return null;
                    }
                    catch (IOException e){ return null;}
                    return bitmap;
                }
            }.execute(string).get();
        }
        catch (ExecutionException ee){
            return null;
        }
        catch(InterruptedException ie){
            return null;
        }
    }
    public static void imageDownload(Context ctx, String url, String name){

        Picasso.get()
                .load(url)
                .into(getTarget(url, name));
    }

    //target to save
    private static Target getTarget(final String url, final String name){
        Target target = new Target(){

            @Override
            public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom from) {
                new Thread(new Runnable() {

                    @Override
                    public void run() {
                        String[] splitName = name.split("/");
                        File myDir = new File(Environment.getExternalStorageDirectory().getPath() + "/sialeg/" + splitName[0] + "/");
                        myDir.mkdirs();
                        //String fname = Calendar.getInstance().getTimeInMillis() + ".png";
                        String fname = splitName[1];
                        File file = new File (myDir, fname);
                        if (file.exists ()) file.delete ();
                        try {
                            //file.createNewFile();
                            FileOutputStream ostream = new FileOutputStream(file);
                            bitmap.compress(Bitmap.CompressFormat.PNG, 80, ostream);
                            ostream.flush();
                            ostream.close();
                        } catch (IOException e) {
                            Log.e(TAG + "IOException", e.getLocalizedMessage());
                        }
                    }
                }).start();

            }

            @Override
            public void onBitmapFailed(Exception e, Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        };
        return target;
    }
}
