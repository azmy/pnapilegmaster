package com.bungker.com.pna.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bungker.com.pna.R;
import com.bungker.com.pna.entities.VoteCaleg;
import com.bungker.com.pna.entities.VoteCalegWithCaleg;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class CalegAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "CalegAdapter";

    private List<VoteCalegWithCaleg> calegs;
    private static final int ITEM=0;
    private static final int LOADING=1;

    private Context context;
    private boolean isLoadingAdded = false;
    private int level;
    TextView nomor;
    private int userid;


    public CalegAdapter(Context context, List<VoteCalegWithCaleg> calegs, int level, int userid ){
        this.context =context;
        this.level = level;
        this.calegs = calegs;
        this.userid = userid;
        System.out.println(TAG + " constructor " );
    }

    public List<VoteCalegWithCaleg>  getData(){
        return calegs;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        Log.i("Info ", "RecyclleView.ViewHolder " );
        RecyclerView.ViewHolder viewHolder =null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (i){
            case ITEM:
                viewHolder = getViewHolder(viewGroup, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.fragment_item, viewGroup, false);
                viewHolder = new LoadingVH(v2);
                break;
        }

        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.calegrow, parent, false);
        viewHolder = new CalegVH(v1);

        nomor =(TextView) v1.findViewById(R.id.nmr);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        VoteCalegWithCaleg caleg =calegs.get(i);


        switch (getItemViewType(i)){
            case ITEM:

                CalegVH partyVH =(CalegVH) viewHolder;

                partyVH.namaPartai.setText(caleg.caleg.getNama());
                Picasso
                        .get()
                        .load(new File(Environment.getExternalStorageDirectory().getPath() + "/sialeg/fotocalegs/" + caleg.caleg.getCalegImage()))
                        .into(partyVH.civGambarPartai);

                for(VoteCaleg vt : caleg.vcalegs){

                    if(vt.getLevel()==level && vt.getJlhSuara()!=0 && vt.getUserId() == userid) {

                        partyVH.inputSuara.setText(Integer.toString(vt.getJlhSuara()));
                    }
                    nomor.setText(String.valueOf(i+1)+".");
               }

                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return calegs==null  ? 0 : calegs.size();
    }

    public void add(VoteCalegWithCaleg mc) {
        calegs.add(mc);
        notifyItemInserted(calegs.size() - 1);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new VoteCalegWithCaleg());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = calegs.size() - 1;
        VoteCalegWithCaleg item = getItem(position);

        if (item != null) {
            calegs.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(VoteCalegWithCaleg party) {
        int position = calegs.indexOf(party);
        if (position > -1) {
            calegs.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public VoteCalegWithCaleg getItem(int position) {return calegs.get(position);}

    protected class CalegVH extends RecyclerView.ViewHolder {

        private TextView namaPartai;
        private CircleImageView civGambarPartai;
        private EditText inputSuara;
        private TextView nomor;


        public CalegVH(@NonNull View itemView) {
            super(itemView);

            namaPartai = (TextView) itemView.findViewById(R.id.namaCaleg);
            civGambarPartai = itemView.findViewById(R.id.imgCaleg);
            inputSuara = (EditText) itemView.findViewById(R.id.suara_caleg);
            nomor = (TextView) itemView.findViewById(R.id.nmr);
            inputSuara.addTextChangedListener(new TextWatcher() {

                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    //editModelArrayList.get(getAdapterPosition()).setEditTextValue(editText.getText().toString());
                    //calegs.get(getAdapterPosition()).vcalegs.get(level).setJlhSuara(Integer.parseInt(inputSuara.getText().toString()));
                    for(VoteCaleg vt : calegs.get(getAdapterPosition()).vcalegs){
                        if(vt.getLevel()==level && vt.getUserId() == userid){
                            String sInputSuara = ("".equals(inputSuara.getText().toString())  ) ? "0" : inputSuara.getText().toString();
                            vt.setJlhSuara(Integer.parseInt(sInputSuara));
                            vt.setIsSent(0);
                        }

                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
        }
    }

    protected LayerDrawable getBorders(int bgColor, int borderColor,
                                       int left, int top, int right, int bottom){
        // Initialize new color drawables
        ColorDrawable borderColorDrawable = new ColorDrawable(borderColor);
        ColorDrawable backgroundColorDrawable = new ColorDrawable(bgColor);

        // Initialize a new array of drawable objects
        Drawable[] drawables = new Drawable[]{
                borderColorDrawable,
                backgroundColorDrawable
        };

        // Initialize a new layer drawable instance from drawables array
        LayerDrawable layerDrawable = new LayerDrawable(drawables);

        // Set padding for background color layer
        layerDrawable.setLayerInset(
                1, // Index of the drawable to adjust [background color layer]
                left, // Number of pixels to add to the left bound [left border]
                top, // Number of pixels to add to the top bound [top border]
                right, // Number of pixels to add to the right bound [right border]
                bottom // Number of pixels to add to the bottom bound [bottom border]
        );

        // Finally, return the one or more sided bordered background drawable
        return layerDrawable;
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }
}