package com.bungker.com.pna.api.services;

import com.bungker.com.pna.api.models.CommonSialeg;
import com.bungker.com.pna.api.models.VoteCaleg;
import com.bungker.com.pna.api.models.VoteParty;

import java.util.List;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;
import retrofit2.http.Path;

public interface SialegSrv {

    @GET("/pemilu/service/setting")
    public Call<CommonSialeg> getSetting();

    @GET("/pemilu/saksi/{telp}")
    public Call<CommonSialeg> getSaksi(@Path("telp") String notelpon);

    @GET("/pemilu/calegs/{dpri}/{dpra}/{dprk}")
    public Call<CommonSialeg> getCaleg(@Path("dpri") int dpri, @Path("dpra") int dpra, @Path("dprk") int dprk);

    @GET("/pemilu/service/profilpartai")
    public Call<CommonSialeg> getPartai();

    @GET("/pemilu/service/vote_calegs")
    public Call<CommonSialeg> getVoteCalegs();

    @GET("/pemilu/service/users/{notelpon}")
    public Call<CommonSialeg> getUser(@Path("notelpon") String notelpon);

    @GET("/pemilu/service/roles")
    public Call<CommonSialeg> getGroup();

    @POST("/pemilu/service/vote_party")
    public Call<CommonSialeg> setVoteParty(@Body List<VoteParty> vcList);

    @POST("/pemilu/service/vote_calegs")
    Call<CommonSialeg> setVoteCaleg(@Body  List<VoteCaleg> vcList);

    @Multipart
    @POST("/pemilu/service/uploads")
    Call<CommonSialeg> uploadSurvey(@Part MultipartBody.Part[] surveyImage, @Part MultipartBody.Part propertyImage, @Part("DRA") RequestBody dra);


}
