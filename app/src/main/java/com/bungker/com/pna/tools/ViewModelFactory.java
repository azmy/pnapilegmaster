package com.bungker.com.pna.tools;


import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.ViewModelProvider;
import android.support.annotation.NonNull;

import com.bungker.com.pna.services.FormC1DataSource;
import com.bungker.com.pna.services.SaksiDataSource;
import com.bungker.com.pna.services.VcwcDatasource;
import com.bungker.com.pna.services.VpwpDataSource;
import com.bungker.com.pna.ui.FormC1ViewModel;
import com.bungker.com.pna.ui.SaksiViewModel;
import com.bungker.com.pna.ui.VcwcViewModel;
import com.bungker.com.pna.ui.VpwpViewModel;


public class ViewModelFactory implements ViewModelProvider.Factory {

    private SaksiDataSource saksiDataSource;
    private  FormC1DataSource formC1DataSource;
    private   String type;

    private VcwcDatasource vcwcDatasource;
    private VpwpDataSource vpwpDataSource;


//    public ViewModelFactory(SaksiDataSource dataSource){
//        this.saksiDataSource = dataSource;
//    }



    public VpwpDataSource getVpwpDataSource() {
        return vpwpDataSource;
    }

    public void setVpwpDataSource(VpwpDataSource vpwpDataSource) {
        this.vpwpDataSource = vpwpDataSource;
    }


    public ViewModelFactory(String type) {
        this.type = type;
    }

    public void setSaksiDataSource(SaksiDataSource saksiDataSource) {
        this.saksiDataSource = saksiDataSource;
    }

    public void setFormC1DataSource(FormC1DataSource formC1DataSource) {
        this.formC1DataSource = formC1DataSource;
    }


    public void setVcwcDatasource(VcwcDatasource vcwcDatasource){
        this.vcwcDatasource = vcwcDatasource;
    }

    public SaksiDataSource getSaksiDataSource() {
        return saksiDataSource;
    }

    public FormC1DataSource getFormC1DataSource() {
        return formC1DataSource;
    }


    public VcwcDatasource getVcwcDatasource() {return vcwcDatasource;}


    @NonNull
    @Override
    public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
        if(modelClass.isAssignableFrom(SaksiViewModel.class)){
            return (T) new SaksiViewModel(saksiDataSource);
        }
        else if(modelClass.isAssignableFrom(FormC1ViewModel.class)){
            return (T) new FormC1ViewModel(formC1DataSource);
        }

        else if(modelClass.isAssignableFrom(VcwcViewModel.class)) {
            return (T) new VcwcViewModel(vcwcDatasource);
        }
        else if(modelClass.isAssignableFrom(VpwpViewModel.class)){
            return (T) new VpwpViewModel(vpwpDataSource);
        }
        throw new IllegalArgumentException("ViewModel tidak dikenali");
    }
}
