package com.bungker.com.pna.api.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommonSialeg {

    @SerializedName("total")
    @Expose
    private Integer total;

    @SerializedName("status")
    @Expose
    private String status;

    @SerializedName("data")
    @Expose
    private List<Object> data;

    @SerializedName("info")
    @Expose
    private Info info;


    @SerializedName("message")
    @Expose
    private String message;


    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    public List<Object> getDatas() {
        return data;
    }

    public void setDatas(List<Object> datas) {
        this.data = datas;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public <T> List<T> toList(Class<T> desiredClass) {
        List<T> transformedList = new ArrayList<>();
        if (getDatas() != null) {
            for (Object result : getDatas()) {
                String json = new Gson().toJson(result);
                T model = new Gson().fromJson(json, desiredClass);
                transformedList.add(model);
            }
        }
        return transformedList;
    }
}
