package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dprri {

    @SerializedName("IDDapil")
    @Expose
    private Integer iDDapil;
    @SerializedName("TingkatDapil")
    @Expose
    private String tingkatDapil;
    @SerializedName("NamaProvinsi")
    @Expose
    private String namaProvinsi;
    @SerializedName("JumlahKursi")
    @Expose
    private Integer jumlahKursi;
    @SerializedName("MinimumKursi")
    @Expose
    private Integer minimumKursi;
    @SerializedName("MaksimumKursi")
    @Expose
    private Integer maksimumKursi;
    @SerializedName("NamaDapil")
    @Expose
    private String namaDapil;
    @SerializedName("AlokasiKursi")
    @Expose
    private Integer alokasiKursi;
    @SerializedName("Wilayah")
    @Expose
    private String wilayah;
    @SerializedName("URL")
    @Expose
    private String uRL;

    public Integer getIDDapil() {
        return iDDapil;
    }

    public void setIDDapil(Integer iDDapil) {
        this.iDDapil = iDDapil;
    }

    public String getTingkatDapil() {
        return tingkatDapil;
    }

    public void setTingkatDapil(String tingkatDapil) {
        this.tingkatDapil = tingkatDapil;
    }

    public String getNamaProvinsi() {
        return namaProvinsi;
    }

    public void setNamaProvinsi(String namaProvinsi) {
        this.namaProvinsi = namaProvinsi;
    }

    public Integer getJumlahKursi() {
        return jumlahKursi;
    }

    public void setJumlahKursi(Integer jumlahKursi) {
        this.jumlahKursi = jumlahKursi;
    }

    public Integer getMinimumKursi() {
        return minimumKursi;
    }

    public void setMinimumKursi(Integer minimumKursi) {
        this.minimumKursi = minimumKursi;
    }

    public Integer getMaksimumKursi() {
        return maksimumKursi;
    }

    public void setMaksimumKursi(Integer maksimumKursi) {
        this.maksimumKursi = maksimumKursi;
    }

    public String getNamaDapil() {
        return namaDapil;
    }

    public void setNamaDapil(String namaDapil) {
        this.namaDapil = namaDapil;
    }

    public Integer getAlokasiKursi() {
        return alokasiKursi;
    }

    public void setAlokasiKursi(Integer alokasiKursi) {
        this.alokasiKursi = alokasiKursi;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

}