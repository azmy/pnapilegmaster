package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProfilePartai {

    @SerializedName("NomorUrutPartai")
    @Expose
    private Integer nomorUrutPartai;
    @SerializedName("NamaPartai")
    @Expose
    private String namaPartai;
    @SerializedName("AkronimNamaPartai")
    @Expose
    private String akronimNamaPartai;
    @SerializedName("AlamatKantor")
    @Expose
    private String alamatKantor;
    @SerializedName("Email")
    @Expose
    private String email;
    @SerializedName("Website")
    @Expose
    private String website;
    @SerializedName("NamaNotaris")
    @Expose
    private String namaNotaris;
    @SerializedName("AlamatNotaris")
    @Expose
    private String alamatNotaris;
    @SerializedName("NomorAktaNotaris")
    @Expose
    private String nomorAktaNotaris;
    @SerializedName("TanggalAkteNotaris")
    @Expose
    private String tanggalAkteNotaris;
    @SerializedName("PersentaseKeterwakilanPerempuan")
    @Expose
    private String persentaseKeterwakilanPerempuan;
    @SerializedName("NomorSk")
    @Expose
    private String nomorSk;
    @SerializedName("KetuaUmum")
    @Expose
    private String ketuaUmum;
    @SerializedName("SekretarisJenderal")
    @Expose
    private String sekretarisJenderal;
    @SerializedName("BendaharaUmum")
    @Expose
    private String bendaharaUmum;
    @SerializedName("Link")
    @Expose
    private String link;

    public Integer getNomorUrutPartai() {
        return nomorUrutPartai;
    }

    public void setNomorUrutPartai(Integer nomorUrutPartai) {
        this.nomorUrutPartai = nomorUrutPartai;
    }

    public String getNamaPartai() {
        return namaPartai;
    }

    public void setNamaPartai(String namaPartai) {
        this.namaPartai = namaPartai;
    }

    public String getAkronimNamaPartai() {
        return akronimNamaPartai;
    }

    public void setAkronimNamaPartai(String akronimNamaPartai) {
        this.akronimNamaPartai = akronimNamaPartai;
    }

    public String getAlamatKantor() {
        return alamatKantor;
    }

    public void setAlamatKantor(String alamatKantor) {
        this.alamatKantor = alamatKantor;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNamaNotaris() {
        return namaNotaris;
    }

    public void setNamaNotaris(String namaNotaris) {
        this.namaNotaris = namaNotaris;
    }

    public String getAlamatNotaris() {
        return alamatNotaris;
    }

    public void setAlamatNotaris(String alamatNotaris) {
        this.alamatNotaris = alamatNotaris;
    }

    public String getNomorAktaNotaris() {
        return nomorAktaNotaris;
    }

    public void setNomorAktaNotaris(String nomorAktaNotaris) {
        this.nomorAktaNotaris = nomorAktaNotaris;
    }

    public String getTanggalAkteNotaris() {
        return tanggalAkteNotaris;
    }

    public void setTanggalAkteNotaris(String tanggalAkteNotaris) {
        this.tanggalAkteNotaris = tanggalAkteNotaris;
    }

    public String getPersentaseKeterwakilanPerempuan() {
        return persentaseKeterwakilanPerempuan;
    }

    public void setPersentaseKeterwakilanPerempuan(String persentaseKeterwakilanPerempuan) {
        this.persentaseKeterwakilanPerempuan = persentaseKeterwakilanPerempuan;
    }

    public String getNomorSk() {
        return nomorSk;
    }

    public void setNomorSk(String nomorSk) {
        this.nomorSk = nomorSk;
    }

    public String getKetuaUmum() {
        return ketuaUmum;
    }

    public void setKetuaUmum(String ketuaUmum) {
        this.ketuaUmum = ketuaUmum;
    }

    public String getSekretarisJenderal() {
        return sekretarisJenderal;
    }

    public void setSekretarisJenderal(String sekretarisJenderal) {
        this.sekretarisJenderal = sekretarisJenderal;
    }

    public String getBendaharaUmum() {
        return bendaharaUmum;
    }

    public void setBendaharaUmum(String bendaharaUmum) {
        this.bendaharaUmum = bendaharaUmum;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

}