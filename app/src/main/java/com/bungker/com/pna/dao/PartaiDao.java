package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.Partai;
import com.bungker.com.pna.entities.PartaiWithCaleg;
import com.bungker.com.pna.entities.VotePartyWithParty;

import java.util.List;
import io.reactivex.Flowable;

@Dao
public interface PartaiDao {

    @Query("select * from parties order by id")
    List<Partai> all();

    @Transaction
    @Query("select * from parties order by id")
    List<PartaiWithCaleg> loadPartaiWithCaleg();

    @Query("select * from parties where id=:id")
    Partai findByOne(int id);

    @Query("select * from parties where nama=:name")
    Partai findByName(String name);

    @Insert
    void insertAll(Partai... partai);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Partai partai);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(Partai partai);
    @Delete
    void delete(Partai user);

    @Transaction
    @Query("select * from vote_partai JOIN parties on vote_partai.party_id = parties.id where level=:level order by level" )
    List<VotePartyWithParty> loadVotePartyWithPartyByLevel(int level);

    @Transaction
    @Query("select * from vote_partai JOIN parties on vote_partai.party_id = parties.id where level=:level order by level" )
    Flowable<List<VotePartyWithParty>> vPwpLoadLiveDataByLevel(int level);

}
