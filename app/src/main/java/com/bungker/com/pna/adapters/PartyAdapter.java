package com.bungker.com.pna.adapters;

import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.LayerDrawable;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.bungker.com.pna.R;
import com.bungker.com.pna.entities.VoteParty;
import com.bungker.com.pna.entities.VotePartyWithParty;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class PartyAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    static final String TAG = "PartyAdapter";

    private List<VotePartyWithParty> parties;
    private static final int ITEM=0;
    private static final int LOADING=1;

    private Context context;
    private boolean isLoadingAdded = false;
    private int level;
    private TextView nomor;
    private int userid;

    public PartyAdapter(Context context, List<VotePartyWithParty> parties,int level, int userid){
        this.context = context;
        this.parties = parties;
        this.level = level;
        this.userid = userid;
        System.out.println(TAG + " constructor " + parties.size());
    }

    public List<VotePartyWithParty>  getData(){
        return parties;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        RecyclerView.ViewHolder viewHolder =null;
        LayoutInflater inflater = LayoutInflater.from(viewGroup.getContext());

        switch (i){
            case ITEM:
                viewHolder = getViewHolder(viewGroup, inflater);
                break;
            case LOADING:
                View v2 = inflater.inflate(R.layout.fragment_item, viewGroup, false);
                viewHolder = new LoadingVH(v2);
                break;
        }

        return viewHolder;
    }

    @NonNull
    private RecyclerView.ViewHolder getViewHolder(ViewGroup parent, LayoutInflater inflater) {
        RecyclerView.ViewHolder viewHolder;
        View v1 = inflater.inflate(R.layout.partairow, parent, false);
        viewHolder = new PartyVH(v1);

        nomor =(TextView) v1.findViewById(R.id.nmr);

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {

        VotePartyWithParty party =parties.get(i);

        switch (getItemViewType(i)){
            case ITEM:
                PartyVH partyVH =(PartyVH) viewHolder;
                partyVH.namaPartai.setText(party.party.getNama());
                Picasso
                        .get()
                        .load(new File(Environment.getExternalStorageDirectory().getPath() + "/sialeg/fotopartai/" + party.party.getGambar()))
                        .into(partyVH.civGambarPartai);
                for(VoteParty vt : party.partaiList){
                    if(vt.getLevel()==level && vt.getJlhSuara()!=0 && vt.getUserId() == userid) {
                        partyVH.inputSuara.setText(Integer.toString(vt.getJlhSuara()));
                    }
                   nomor.setText(String.valueOf(vt.getPartyId())+".");
                }

                break;
            case LOADING:
                break;
        }
    }

    @Override
    public int getItemCount() {
        return parties==null  ? 0 : parties.size();
    }

    public void add(VotePartyWithParty mc) {
        parties.add(mc);
        notifyItemInserted(parties.size() - 1);
    }

    public void addLoadingFooter() {
        isLoadingAdded = true;
        add(new VotePartyWithParty());
    }

    public void removeLoadingFooter() {
        isLoadingAdded = false;

        int position = parties.size() - 1;
        VotePartyWithParty item = getItem(position);

        if (item != null) {
            parties.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void remove(VotePartyWithParty party) {
        int position = parties.indexOf(party);
        if (position > -1) {
            parties.remove(position);
            notifyItemRemoved(position);
        }
    }

    public void clear() {
        isLoadingAdded = false;
        while (getItemCount() > 0) {
            remove(getItem(0));
        }
    }

    public VotePartyWithParty getItem(int position) {return parties.get(position);}

    protected class PartyVH extends RecyclerView.ViewHolder {
        private TextView namaPartai;
        private CircleImageView civGambarPartai;
        private EditText inputSuara;

        public PartyVH(@NonNull View itemView) {
            super(itemView);
            namaPartai = (TextView) itemView.findViewById(R.id.namaPartai);
            civGambarPartai = itemView.findViewById(R.id.imgPartai);
            inputSuara = itemView.findViewById(R.id.suara_partai);

            inputSuara.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    //editModelArrayList.get(getAdapterPosition()).setEditTextValue(editText.getText().toString());
                    for(VoteParty vt : parties.get(getAdapterPosition()).partaiList){
                        if(vt.getLevel()==level && vt.getUserId()==userid){

                            String sInputSuara = ("".equals(inputSuara.getText().toString())  ) ? "0" : inputSuara.getText().toString();
                            vt.setJlhSuara(Integer.parseInt(sInputSuara));
                            vt.setIsSent(0);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable editable) {

                }
            });
//            LayerDrawable bottomBorder = getBorders(
//                    Color.WHITE, // Background color
//                    Color.GRAY, // Border color
//                    0, // Left border in pixels
//                    0, // Top border in pixels
//                    0, // Right border in pixels
//                    1 // Bottom border in pixels
//            );
//
//            if (Build.VERSION.SDK_INT >= 16) {
//                namaPartai.setBackground(bottomBorder);
//            }
        }
    }

    protected LayerDrawable getBorders(int bgColor, int borderColor,
                                       int left, int top, int right, int bottom){
        // Initialize new color drawables
        ColorDrawable borderColorDrawable = new ColorDrawable(borderColor);
        ColorDrawable backgroundColorDrawable = new ColorDrawable(bgColor);

        // Initialize a new array of drawable objects
        Drawable[] drawables = new Drawable[]{
                borderColorDrawable,
                backgroundColorDrawable
        };

        // Initialize a new layer drawable instance from drawables array
        LayerDrawable layerDrawable = new LayerDrawable(drawables);

        // Set padding for background color layer
        layerDrawable.setLayerInset(
                1, // Index of the drawable to adjust [background color layer]
                left, // Number of pixels to add to the left bound [left border]
                top, // Number of pixels to add to the top bound [top border]
                right, // Number of pixels to add to the right bound [right border]
                bottom // Number of pixels to add to the bottom bound [bottom border]
        );

        // Finally, return the one or more sided bordered background drawable
        return layerDrawable;
    }

    protected class LoadingVH extends RecyclerView.ViewHolder {
        public LoadingVH(View itemView) {
            super(itemView);
        }
    }

}
