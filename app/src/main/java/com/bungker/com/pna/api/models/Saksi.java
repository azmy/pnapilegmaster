package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Saksi {
    @SerializedName("tps_id")
    @Expose
    private Integer tpsId;
    @SerializedName("saksi_id")
    @Expose
    private Integer saksiId;
    @SerializedName("name")
    @Expose
    private String name;
    @SerializedName("address")
    @Expose
    private String address;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("user_id")
    @Expose
    private Integer userId;
    @SerializedName("no_tlpn")
    @Expose
    private String noTlpn;
    @SerializedName("kecamatan")
    @Expose
    private String kecamatan;
    @SerializedName("kelurahan")
    @Expose
    private String kelurahan;
    @SerializedName("kabupaten")
    @Expose
    private String kabupaten;
    @SerializedName("nourutpartai")
    @Expose
    private Integer nourutpartai;

    @SerializedName("tps")
    @Expose
    private String tps;

    @SerializedName("dapildpri_id")
    @Expose
    private Integer dapildpriId;
    @SerializedName("dapildpra_id")
    @Expose
    private Integer dapildpraId;
    @SerializedName("dapildprk_id")
    @Expose
    private Integer dapildprkId;

    public Integer getTpsId() {
        return tpsId;
    }

    public void setTpsId(Integer tpsId) {
        this.tpsId = tpsId;
    }

    public Integer getSaksiId() {
        return saksiId;
    }

    public void setSaksiId(Integer saksiId) {
        this.saksiId = saksiId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getNoTlpn() {
        return noTlpn;
    }

    public void setNoTlpn(String noTlpn) {
        this.noTlpn = noTlpn;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public Integer getNourutpartai() {
        return nourutpartai;
    }

    public void setNourutpartai(Integer nourutpartai) {
        this.nourutpartai = nourutpartai;
    }

    public String getTps() {
        return tps;
    }

    public void setTps(String tps) {
        this.tps = tps;
    }

    public Integer getDapildpriId() {
        return dapildpriId;
    }

    public void setDapildpriId(Integer dapildpriId) {
        this.dapildpriId = dapildpriId;
    }

    public Integer getDapildpraId() {
        return dapildpraId;
    }

    public void setDapildpraId(Integer dapildpraId) {
        this.dapildpraId = dapildpraId;
    }

    public Integer getDapildprkId() {
        return dapildprkId;
    }

    public void setDapildprkId(Integer dapildprkId) {
        this.dapildprkId = dapildprkId;
    }

}
