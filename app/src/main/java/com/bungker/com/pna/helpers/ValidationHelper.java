package com.bungker.com.pna.helpers;

import android.util.Log;

import com.bungker.com.pna.helpers.annotations.NotEmpty;

import java.lang.reflect.Field;
import java.util.HashMap;
import java.util.Map;

import static java.util.Objects.requireNonNull;

public class ValidationHelper {

    private static String TAG = "ValidationHelper";

    public String serialize(Object object) throws Exception{
        try{
            Class<?> objectClass = requireNonNull(object).getClass();
            Map<String, String> jsonElements = new HashMap<>();
            for (Field field: objectClass.getDeclaredFields()) {
                field.setAccessible(true);
                if (field.isAnnotationPresent(NotEmpty.class)) {
                    jsonElements.put(getSerializedKey(field), (String) field.get(object));
                }
            }
            System.out.println(toJsonString(jsonElements));
            return toJsonString(jsonElements);
        }
        catch (Exception e){
            Log.e(TAG, "Exeption serialize : " + e.getMessage());
            return "";
        }

    }
    private String toJsonString(Map<String, String> jsonMap) {
        return "";
    }



    private static String getSerializedKey(Field field) {
        String annotationValue = field.getAnnotation(NotEmpty.class).value();
        if (annotationValue.isEmpty()) {
            return field.getName();
        }
        else {
            return annotationValue;
        }
    }
}
