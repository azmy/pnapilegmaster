package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DCTDPRDProvinsi {

    @SerializedName("Tingkat")
    @Expose
    private String tingkat;
    @SerializedName("Provinsi")
    @Expose
    private String provinsi;
    @SerializedName("DaerahPemilihan")
    @Expose
    private String daerahPemilihan;
    @SerializedName("NamaPartai")
    @Expose
    private String namaPartai;
    @SerializedName("NomorUrut")
    @Expose
    private String nomorUrut;
    @SerializedName("IDCaleg")
    @Expose
    private Integer iDCaleg;
    @SerializedName("NamaLengkap")
    @Expose
    private String namaLengkap;
    @SerializedName("JenisKelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("TempatTinggal")
    @Expose
    private String tempatTinggal;
    @SerializedName("Detail")
    @Expose
    private String detail;
    @SerializedName("URLId")
    @Expose
    private String uRLId;

    public String getTingkat() {
        return tingkat;
    }

    public void setTingkat(String tingkat) {
        this.tingkat = tingkat;
    }

    public String getProvinsi() {
        return provinsi;
    }

    public void setProvinsi(String provinsi) {
        this.provinsi = provinsi;
    }

    public String getDaerahPemilihan() {
        return daerahPemilihan;
    }

    public void setDaerahPemilihan(String daerahPemilihan) {
        this.daerahPemilihan = daerahPemilihan;
    }

    public String getNamaPartai() {
        return namaPartai;
    }

    public void setNamaPartai(String namaPartai) {
        this.namaPartai = namaPartai;
    }

    public String getNomorUrut() {
        return nomorUrut;
    }

    public void setNomorUrut(String nomorUrut) {
        this.nomorUrut = nomorUrut;
    }

    public Integer getIDCaleg() {
        return iDCaleg;
    }

    public void setIDCaleg(Integer iDCaleg) {
        this.iDCaleg = iDCaleg;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatTinggal() {
        return tempatTinggal;
    }

    public void setTempatTinggal(String tempatTinggal) {
        this.tempatTinggal = tempatTinggal;
    }

    public String getDetail() {
        return detail;
    }

    public void setDetail(String detail) {
        this.detail = detail;
    }

    public String getURLId() {
        return uRLId;
    }

    public void setURLId(String uRLId) {
        this.uRLId = uRLId;
    }

}