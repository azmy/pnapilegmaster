package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Partai {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("nourutpartai")
    @Expose
    private String nourutpartai;
    @SerializedName("namapartai")
    @Expose
    private String namapartai;
    @SerializedName("akronimpartai")
    @Expose
    private String akronimpartai;
    @SerializedName("image")
    @Expose
    private String image;
    @SerializedName("alamat")
    @Expose
    private String alamat;
    @SerializedName("email")
    @Expose
    private String email;
    @SerializedName("website")
    @Expose
    private String website;
    @SerializedName("notaris")
    @Expose
    private String notaris;
    @SerializedName("alamatnotaris")
    @Expose
    private String alamatnotaris;
    @SerializedName("noaktanotaris")
    @Expose
    private String noaktanotaris;
    @SerializedName("tglaktanotaris")
    @Expose
    private String tglaktanotaris;
    @SerializedName("pesentaseketerwakilanperempuan")
    @Expose
    private String pesentaseketerwakilanperempuan;
    @SerializedName("nosk")
    @Expose
    private String nosk;
    @SerializedName("ketuaumum")
    @Expose
    private String ketuaumum;
    @SerializedName("sekjendral")
    @Expose
    private String sekjendral;
    @SerializedName("bendaharaumum")
    @Expose
    private String bendaharaumum;
    @SerializedName("link")
    @Expose
    private String link;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNourutpartai() {
        return nourutpartai;
    }

    public void setNourutpartai(String nourutpartai) {
        this.nourutpartai = nourutpartai;
    }

    public String getNamapartai() {
        return namapartai;
    }

    public void setNamapartai(String namapartai) {
        this.namapartai = namapartai;
    }

    public String getAkronimpartai() {
        return akronimpartai;
    }

    public void setAkronimpartai(String akronimpartai) {
        this.akronimpartai = akronimpartai;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getNotaris() {
        return notaris;
    }

    public void setNotaris(String notaris) {
        this.notaris = notaris;
    }

    public String getAlamatnotaris() {
        return alamatnotaris;
    }

    public void setAlamatnotaris(String alamatnotaris) {
        this.alamatnotaris = alamatnotaris;
    }

    public String getNoaktanotaris() {
        return noaktanotaris;
    }

    public void setNoaktanotaris(String noaktanotaris) {
        this.noaktanotaris = noaktanotaris;
    }

    public String getTglaktanotaris() {
        return tglaktanotaris;
    }

    public void setTglaktanotaris(String tglaktanotaris) {
        this.tglaktanotaris = tglaktanotaris;
    }

    public String getPesentaseketerwakilanperempuan() {
        return pesentaseketerwakilanperempuan;
    }

    public void setPesentaseketerwakilanperempuan(String pesentaseketerwakilanperempuan) {
        this.pesentaseketerwakilanperempuan = pesentaseketerwakilanperempuan;
    }

    public String getNosk() {
        return nosk;
    }

    public void setNosk(String nosk) {
        this.nosk = nosk;
    }

    public String getKetuaumum() {
        return ketuaumum;
    }

    public void setKetuaumum(String ketuaumum) {
        this.ketuaumum = ketuaumum;
    }

    public String getSekjendral() {
        return sekjendral;
    }

    public void setSekjendral(String sekjendral) {
        this.sekjendral = sekjendral;
    }

    public String getBendaharaumum() {
        return bendaharaumum;
    }

    public void setBendaharaumum(String bendaharaumum) {
        this.bendaharaumum = bendaharaumum;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }
}
