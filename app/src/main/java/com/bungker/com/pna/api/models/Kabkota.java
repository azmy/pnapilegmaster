package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Kabkota {
    @SerializedName("IDDapil")
    @Expose
    private String iDDapil;

    @SerializedName("TingkatDapil")
    @Expose
    private String tingkatDapil;

    @SerializedName("NamaProvinsi")
    @Expose
    private String namaProvinsi;

    @SerializedName("NamaKabupaten")
    @Expose
    private String namaKabupaten;

    @SerializedName("BPPD")
    @Expose
    private String bPPD;

    @SerializedName("JumlahKursi")
    @Expose
    private String jumlahKursi;

    @SerializedName("MinimumKursi")
    @Expose
    private String minimumKursi;

    @SerializedName("MaksimumKursi")
    @Expose
    private String maksimumKursi;

    @SerializedName("NamaDapil")
    @Expose
    private String namaDapil;

    @SerializedName("AlokasiKursi")
    @Expose
    private String alokasiKursi;

    @SerializedName("Wilayah")
    @Expose
    private String wilayah;

    @SerializedName("URL")
    @Expose
    private String uRL;

    public String getIDDapil() {
        return iDDapil;
    }

    public void setIDDapil(String iDDapil) {
        this.iDDapil = iDDapil;
    }

    public String getTingkatDapil() {
        return tingkatDapil;
    }

    public void setTingkatDapil(String tingkatDapil) {
        this.tingkatDapil = tingkatDapil;
    }

    public String getNamaProvinsi() {
        return namaProvinsi;
    }

    public void setNamaProvinsi(String namaProvinsi) {
        this.namaProvinsi = namaProvinsi;
    }

    public String getNamaKabupaten() {
        return namaKabupaten;
    }

    public void setNamaKabupaten(String namaKabupaten) {
        this.namaKabupaten = namaKabupaten;
    }

    public String getBPPD() {
        return bPPD;
    }

    public void setBPPD(String bPPD) {
        this.bPPD = bPPD;
    }

    public String getJumlahKursi() {
        return jumlahKursi;
    }

    public void setJumlahKursi(String jumlahKursi) {
        this.jumlahKursi = jumlahKursi;
    }

    public String getMinimumKursi() {
        return minimumKursi;
    }

    public void setMinimumKursi(String minimumKursi) {
        this.minimumKursi = minimumKursi;
    }

    public String getMaksimumKursi() {
        return maksimumKursi;
    }

    public void setMaksimumKursi(String maksimumKursi) {
        this.maksimumKursi = maksimumKursi;
    }

    public String getNamaDapil() {
        return namaDapil;
    }

    public void setNamaDapil(String namaDapil) {
        this.namaDapil = namaDapil;
    }

    public String getAlokasiKursi() {
        return alokasiKursi;
    }

    public void setAlokasiKursi(String alokasiKursi) {
        this.alokasiKursi = alokasiKursi;
    }

    public String getWilayah() {
        return wilayah;
    }

    public void setWilayah(String wilayah) {
        this.wilayah = wilayah;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }
}
