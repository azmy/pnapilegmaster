package com.bungker.com.pna.api.tools;

import android.support.annotation.NonNull;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ApiPemilu {
    private static Retrofit retrofit;
    private static String BASE_URL="http://api.demokrataceh.id/";

    private ApiPemilu(){}

    private final static OkHttpClient client = buildClient();

    @NonNull
    protected static OkHttpClient buildClient(){
        OkHttpClient.Builder builder = new OkHttpClient.Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request request = chain.request();

                        Request.Builder builder = request.newBuilder()
                                .addHeader("Accept", "application/x-www-form-urlencoded")
                                .addHeader("Authorization", "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.e30.t-IDcSemACt8x4iTMCda8Yhe3iZaWbvV5XKSTbuAn0M");

                        request = builder.build();

                        return chain.proceed(request);
                    }
                });

        return builder.build();

    }

    public static Retrofit getRetrofit(){
        Gson gson = new GsonBuilder()
                .setLenient()
                .create();

        if(retrofit==null) {

            retrofit = new Retrofit
                    .Builder()
                    .baseUrl(BASE_URL)
                    .client(client)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .build();

        }
        return retrofit;
    }

    public static <T> T createService(Class<T> service){
        return getRetrofit().create(service);
    }


    public static  ApiPemilu newIntance(){
        return ClassIntance.INSTANCE;
    }


    public static class ClassIntance {
        private  static final ApiPemilu INSTANCE = new ApiPemilu();
    }


}
