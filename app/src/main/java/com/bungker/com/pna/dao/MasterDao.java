package com.bungker.com.pna.dao;

import java.util.List;

public interface MasterDao<T> {

    long insert(T t);
    int  update(T t);
    List<T> all();
    T findByOne(int i);
    T FindByName(String  str);
}
