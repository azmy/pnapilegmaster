package com.bungker.com.pna.adapters;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bungker.com.pna.DetailC1;
import com.bungker.com.pna.R;
import com.bungker.com.pna.entities.FormC1;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.List;

public class GalleriesAdapter extends BaseAdapter {
    private Context context;
    private List<FormC1> arrayList;
    ImageView imageView;
    public GalleriesAdapter(Context context,List<FormC1> arrayList) {
        this.context = context;
        this.arrayList = arrayList;
    }
    public void addData(FormC1 p){
        arrayList.add(p);
        notifyDataSetChanged();
    }
    public void addListData(List<FormC1> parties){
        this.arrayList = parties;
        notifyDataSetChanged();
    }
    public List<FormC1> getData(){
        return arrayList;
    }
    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return arrayList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (mInflater != null) {
            convertView = mInflater.inflate(R.layout.list_galeries, parent, false);
        }
        ImageView imageView = convertView.findViewById(R.id.imageView);
        TextView imagePath = convertView.findViewById(R.id.imagePath);
        
        //imagePath.setText(FileUtils.getPath(context, arrayList.get(position)));

//        Glide.with(context)
//                .load(arrayList.get(position).getGambar())
//                .into(imageView);
        Picasso
                .get()
                .load(new File(arrayList.get(position).getImagePath()))
                .into(imageView);

        imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               System.out.println(arrayList.get(position).getImagePath());
                Intent intent = new Intent(context, DetailC1.class);
                intent.putExtra("uri", arrayList.get(position).getImagePath());
                intent.putExtra("id", arrayList.get(position).getId());
                intent.putExtra("position", position);
                context.startActivity(intent);


            }
        });

        return convertView;
    }

    public void removeItem(int position){
        arrayList.remove(position);
        notifyDataSetChanged();


    }


}
