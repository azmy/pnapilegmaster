package com.bungker.com.pna.dao;


import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Query;

@Dao
public interface CheckVoteDao {


    @Query("SELECT COUNT(*) from vote_partai where is_sent='1'")
    int countVotePartai();

    @Query("SELECT COUNT(*) from vote_caleg where is_sent='1'")
    int countVoteCaleg();

    @Query("SELECT COUNT(*) from vote_caleg where is_sent='0'")
    int countVoteCalegNotSent();

    @Query("SELECT COUNT(*) from vote_partai where is_sent='0'")
    int countVotePartaiNotSent();


}
