package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoteCaleg {
    @SerializedName("id")
    @Expose
    transient  private String id;

    @SerializedName("tps_id")
    @Expose
    private String tpsId;
    @SerializedName("jumlah_suara")
    @Expose
    private String jumlahSuara;
    @SerializedName("caleg_id")
    @Expose
    private String calegId;
    @SerializedName("user_id")
    @Expose
    private String userId;
    @SerializedName("level")
    @Expose
    private String level;

    @SerializedName("party_id")
    @Expose
    private String partyId;

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTpsId() {
        return tpsId;
    }

    public void setTpsId(String tpsId) {
        this.tpsId = tpsId;
    }

    public String getJumlahSuara() {
        return jumlahSuara;
    }

    public void setJumlahSuara(String jumlahSuara) {
        this.jumlahSuara = jumlahSuara;
    }

    public String getCalegId() {
        return calegId;
    }

    public void setCalegId(String calegId) {
        this.calegId = calegId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
