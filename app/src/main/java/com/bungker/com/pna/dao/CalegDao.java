package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Transaction;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.Caleg;
import com.bungker.com.pna.entities.VoteCalegWithCaleg;

import java.util.List;
import io.reactivex.Flowable;

@Dao
public interface CalegDao {
    @Query("SELECT * FROM calegs")
    List<Caleg> all();

    @Query("SELECT * from calegs where dapildpri_id=:ri or  dapildpra_id=:ra or dapildprk_id=:rk")
    List<Caleg> findByIdDapilAllLevel(int ri, int ra, int rk);

    @Query("SELECT * from calegs where id_caleg=:id")
    Caleg findOne(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Caleg product);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(Caleg stock);

    @Delete
    void delete(Caleg... users);

    @Query("SELECT * FROM calegs where name_caleg=:nama")
    Caleg findByName(String nama);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertAll(Caleg... calegs);

    @Transaction
    @Query("Select * from calegs" +
            " JOIN vote_caleg on calegs.id_caleg = vote_caleg.caleg_id  "
            + " JOIN parties on parties.id = calegs.partai_id where calegs.caleg_level=:level and vote_caleg.user_id=:userid order by calegs.no_urut asc")
    List<VoteCalegWithCaleg> loadPartaiWithCalegByLevel(int level, int userid);

    @Transaction
    @Query("Select * from calegs" +
            " JOIN vote_caleg on calegs.id_caleg = vote_caleg.caleg_id  "
            + " JOIN parties on parties.id = calegs.partai_id where calegs.caleg_level=:level and vote_caleg.user_id=:userid order by calegs.no_urut asc")
    Flowable<List<VoteCalegWithCaleg>> loadRxPartaiWithCalegByLevel(int level, int userid);
}
