package com.bungker.com.pna.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "witnesses")
public class Saksi {

    @PrimaryKey
    int id;

    @ColumnInfo(name="name")
    String name;

    @ColumnInfo(name="address")
    String address;

    @ColumnInfo(name="image")
    String image;

    @ColumnInfo(name="party_id")
    int partyId;

    @ColumnInfo(name="tps_id")
    int tpsId;

    @ColumnInfo(name="nomor_tps")
    int tps;

    @ColumnInfo(name="user_id")
    int users;

    private int dapilId;

    @ColumnInfo(name="dapildprk_id")
    private int dapildprkId;

    @ColumnInfo(name="dapildpra_id")
    private int dapildpraId;

    @ColumnInfo(name="dapildpri_id")
    private int dapildpriId;

    @ColumnInfo(name="kabupaten")
    private String kabupaten;

    @ColumnInfo(name="kecamatan")
    private String kecamatan;

    @ColumnInfo(name="kelurahan")
    private String kelurahan;

    public int getTps() {
        return tps;
    }

    public void setTps(int tps) {
        this.tps = tps;
    }

    public String getKabupaten() {
        return kabupaten;
    }

    public void setKabupaten(String kabupaten) {
        this.kabupaten = kabupaten;
    }

    public String getKecamatan() {
        return kecamatan;
    }

    public void setKecamatan(String kecamatan) {
        this.kecamatan = kecamatan;
    }

    public String getKelurahan() {
        return kelurahan;
    }

    public void setKelurahan(String kelurahan) {
        this.kelurahan = kelurahan;
    }

    public int getDapilId() {
        return dapilId;
    }

    public void setDapilId(int dapilId) {
        this.dapilId = dapilId;
    }

    public int getDapildprkId() {
        return dapildprkId;
    }

    public void setDapildprkId(int dapildprkId) {
        this.dapildprkId = dapildprkId;
    }

    public int getDapildpraId() {
        return dapildpraId;
    }

    public void setDapildpraId(int dapildpraId) {
        this.dapildpraId = dapildpraId;
    }

    public int getDapildpriId() {
        return dapildpriId;
    }

    public void setDapildpriId(int dapildpriId) {
        this.dapildpriId = dapildpriId;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public int getPartyId() {
        return partyId;
    }

    public void setPartyId(int partyId) {
        this.partyId = partyId;
    }

    public int getTpsId() {
        return tpsId;
    }

    public void setTpsId(int tpsId) {
        this.tpsId = tpsId;
    }

    public int getUsers() {
        return users;
    }

    public void setUsers(int users) {
        this.users = users;
    }
}
