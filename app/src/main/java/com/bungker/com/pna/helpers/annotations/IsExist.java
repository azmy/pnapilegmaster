package com.bungker.com.pna.helpers.annotations;

import com.bungker.com.pna.helpers.validations.CheckExist;
import com.mobsandgeeks.saripaar.annotation.ValidateUsing;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@ValidateUsing(CheckExist.class)
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.FIELD)
public @interface IsExist {
    public int messageResId()  default  -1;
    public String message() default "Data sudah ada!";
    public int sequence()       default -1;
    public String key();
}
