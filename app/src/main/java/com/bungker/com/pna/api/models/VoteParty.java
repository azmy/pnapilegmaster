package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VoteParty {

    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("tps_id")
    @Expose
    String tps_id;

    @SerializedName("jumlah_suara")
    @Expose
    String jumlahSuara;

    @SerializedName("party_id")
    @Expose
    String partyId;

    @SerializedName("user_id")
    @Expose
    String userId;

    @SerializedName("level")
    @Expose
    String level;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTps_id() {
        return tps_id;
    }

    public void setTps_id(String tps_id) {
        this.tps_id = tps_id;
    }

    public String getJumlahSuara() {
        return jumlahSuara;
    }

    public void setJumlahSuara(String jumlahSuara) {
        this.jumlahSuara = jumlahSuara;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
