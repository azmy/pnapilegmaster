package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.Setting;

import java.util.List;

@Dao
public interface SettingDao {

    @Insert
    long insert(Setting set);

    @Update
    int update(Setting set);

    @Query("SELECT * FROM settings")
    List<Setting> all();

    @Query("SELECT * FROM settings where id=:id")
    Setting findById(int id);
}
