package com.bungker.com.pna.tools;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import com.bungker.com.pna.api.models.Caleg;
import com.bungker.com.pna.api.models.CommonSialeg;
import com.bungker.com.pna.api.models.Partai;
import com.bungker.com.pna.api.models.Saksi;
import com.bungker.com.pna.api.services.SialegSrv;
import com.bungker.com.pna.api.tools.ApiSiAleg;
import com.bungker.com.pna.constata.SialegConstant;
import com.bungker.com.pna.entities.Group;
import com.bungker.com.pna.entities.Setting;
import com.bungker.com.pna.entities.User;
import com.bungker.com.pna.entities.VoteCaleg;
import com.bungker.com.pna.entities.VoteParty;
import com.bungker.com.pna.helpers.MyHelpers;
import com.bungker.com.pna.services.implement.ICalegService;
import com.bungker.com.pna.services.implement.IGroupService;
import com.bungker.com.pna.services.implement.IPartaiService;
import com.bungker.com.pna.services.implement.ISaksiService;
import com.bungker.com.pna.services.implement.ISettingService;
import com.bungker.com.pna.services.implement.IUserService;
import com.bungker.com.pna.services.implement.IVoteCalegService;
import com.bungker.com.pna.services.implement.IVotePartyService;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataAwal {

    private static final String TAG="DataAwal";
    private Context context;

    private ISaksiService iss;
    private ISettingService iSetService;
    private IPartaiService iPartySrv;
    private ICalegService iCalegSrv;
    private IVoteCalegService ivCalegSrv;
    private IVotePartyService ivPartySrv;
    private IUserService   iUsrSrv;
    private IGroupService iGroupSrv;

    public DataAwal(Context context){
        this.context = context;
        iss = ISaksiService.newIntance(context);
        iSetService = ISettingService.newIntance(context);
        iPartySrv = IPartaiService.newIntance(context);
        iCalegSrv = ICalegService.newIntance(context);
        ivCalegSrv = IVoteCalegService.newIntance(context);
        ivPartySrv = IVotePartyService.newIntance(context);
        iUsrSrv = IUserService.newInstance(context);
        iGroupSrv = IGroupService.newInstance(context);
    }

    public void getSetting(){
        SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);

        Call<CommonSialeg> sialegCall = sourceData.getSetting();
        sialegCall.enqueue(new Callback<CommonSialeg>() {
            @Override
            public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {
                if (response.isSuccessful()) {

                    List<com.bungker.com.pna.api.models.Setting> settList = new ArrayList<>();

                    try {
                        settList = response.body().toList(com.bungker.com.pna.api.models.Setting.class);
                        for (com.bungker.com.pna.api.models.Setting set : settList) {
                            if(iSetService.all().size() ==0) {
                                Setting st = new Setting();
                                st.setId(Integer.parseInt(set.getId()));
                                st.setNama(set.getConfigKey());
                                st.setValue(Integer.parseInt(set.getConfigValue()));
                                iSetService.insert(st);
                            }
                            else {
                                Setting existSt = iSetService.findById(Integer.parseInt(set.getId()));
                                if(existSt==null){
                                    Setting st = new Setting();
                                    st.setId(Integer.parseInt(set.getId()));
                                    st.setNama(set.getConfigKey());
                                    st.setValue(Integer.parseInt(set.getConfigValue()));
                                    iSetService.insert(st);
                                }
                            }
                        }
                    } catch (ClassCastException cce) {
                        Log.e("ClassCastException : ", cce.getMessage());
                    }
                }
            }

            @Override
            public void onFailure(Call<CommonSialeg> call, Throwable t) {

            }
        });
    }

    public void getGroup(){
        if(iGroupSrv.all()  != null && iGroupSrv.all().size()==0){
            SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);

            Call<CommonSialeg> sialegCall = sourceData.getGroup();

            sialegCall.enqueue(new Callback<CommonSialeg>() {
                @Override
                public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {
                    if (response.isSuccessful()) {

                        List<com.bungker.com.pna.api.models.Group> groupList = new ArrayList<>();

                        try {
                            groupList = response.body().toList(com.bungker.com.pna.api.models.Group.class);
                            for (com.bungker.com.pna.api.models.Group group : groupList) {
                                Group gp = new Group();
                                gp.setName(group.getName());
                                gp.setId(Integer.parseInt(group.getId()));
                                iGroupSrv.insert(gp);
                            }
                        } catch (ClassCastException cce) {
                            Log.e("ClassCastException : ", cce.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonSialeg> call, Throwable t) {

                }
            });
        }
    }

    public void getUser(final String notelpon){


        SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);

        if(notelpon!=null) {
            Call<CommonSialeg> sialegCall = sourceData.getUser(notelpon);

            sialegCall.enqueue(new Callback<CommonSialeg>() {
                @Override
                public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {

                    if (response.isSuccessful() && ! response.body().getTotal().equals("0")) {

                        List<com.bungker.com.pna.api.models.User> userList = new ArrayList<>();

                        try {
                            userList = response.body().toList(com.bungker.com.pna.api.models.User.class);
                            if (iUsrSrv.all().size() == 0 && iGroupSrv.all().size() > 0) {
                                for (com.bungker.com.pna.api.models.User usr : userList) {
                                    User user = new User();
                                    System.out.println(TAG + " get user " + usr.getName());
                                    user.setName(usr.getName());
                                    user.setPassword(usr.getPassword());
                                    user.setGroups(Integer.parseInt(usr.getGroupId()));
                                    user.setId(Integer.parseInt(usr.getId()));
                                    user.setNoTelpon(usr.getNoTlpn());
                                    iUsrSrv.insert(user);
                                }
                            } else {
                                User existUser = iUsrSrv.findByTelpon(notelpon);
                                for (com.bungker.com.pna.api.models.User usr : userList) {

                                    if (existUser == null) {
                                        User user = new User();
                                        user.setName(usr.getName());
                                        user.setPassword(usr.getPassword());
                                        user.setGroups(Integer.parseInt(usr.getGroupId()));
                                        user.setId(Integer.parseInt(usr.getId()));
                                        user.setNoTelpon(usr.getNoTlpn());
                                        iUsrSrv.insert(user);
                                    } else {
                                        existUser.setNoTelpon(usr.getName());
                                        existUser.setPassword(usr.getPassword());
                                        existUser.setGroups(Integer.parseInt(usr.getGroupId()));
                                        existUser.setId(Integer.parseInt(usr.getId()));
                                        existUser.setNoTelpon(usr.getNoTlpn());
                                        iUsrSrv.update(existUser);
                                    }
                                }

                            }

                        } catch (ClassCastException cce) {
                            Log.e("ClassCastException : ", cce.getMessage());

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    }
                }

                @Override
                public void onFailure(Call<CommonSialeg> call, Throwable t) {
                    Log.e(TAG, " error " + t.getMessage());
                }
            });
        }

    }

    public void getSaksi(final String notelpon){
        if(notelpon!=null) {
            User u = iUsrSrv.findByTelpon(notelpon);
            if (u != null) {


                final SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);

                Call<CommonSialeg> sialegCall = sourceData.getSaksi(notelpon);
                sialegCall.enqueue(new Callback<CommonSialeg>() {
                    @Override
                    public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {
                        if (response.isSuccessful()) {

                            List<Saksi> saksiList = new ArrayList<>();
                            User u = iUsrSrv.findByTelpon(notelpon);
                            try {
                                saksiList = response.body().toList(Saksi.class);
                                if (u != null) {
                                    if (iss.all().size() == 0) {
                                        for (Saksi saksi : saksiList) {
                                            com.bungker.com.pna.entities.Saksi tblSaksi = new com.bungker.com.pna.entities.Saksi();
                                            tblSaksi.setId(saksi.getSaksiId());
                                            tblSaksi.setImage(saksi.getImage());
                                            tblSaksi.setAddress(saksi.getKelurahan());
                                            tblSaksi.setPartyId(saksi.getNourutpartai());
                                            tblSaksi.setTpsId(saksi.getTpsId());
                                            tblSaksi.setTps(Integer.parseInt(saksi.getTps()));
                                            tblSaksi.setUsers(saksi.getUserId());
                                            tblSaksi.setName(saksi.getName());
                                            tblSaksi.setKecamatan(saksi.getKecamatan());
                                            tblSaksi.setKabupaten(saksi.getKabupaten());
                                            tblSaksi.setKelurahan(saksi.getKelurahan());
                                            tblSaksi.setDapildprkId(saksi.getDapildprkId());
                                            tblSaksi.setDapildpraId(saksi.getDapildpraId());
                                            tblSaksi.setDapildpriId(saksi.getDapildpriId());
                                            iss.insert(tblSaksi);
                                        }
                                    } else {

                                        for (Saksi saksi : saksiList) {
                                            com.bungker.com.pna.entities.Saksi tblSaksi = iss.findById(saksi.getSaksiId());
                                            if (tblSaksi == null) {
                                                tblSaksi = new com.bungker.com.pna.entities.Saksi();
                                                tblSaksi.setId(saksi.getSaksiId());
                                                tblSaksi.setImage(saksi.getImage());
                                                tblSaksi.setAddress(saksi.getAddress());
                                                tblSaksi.setPartyId(saksi.getNourutpartai());
                                                tblSaksi.setTpsId(saksi.getTpsId());
                                                tblSaksi.setTps(Integer.parseInt(saksi.getTps()));
                                                tblSaksi.setUsers(saksi.getUserId());
                                                tblSaksi.setName(saksi.getName());
                                                tblSaksi.setKecamatan(saksi.getKecamatan());
                                                tblSaksi.setKabupaten(saksi.getKabupaten());
                                                tblSaksi.setKelurahan(saksi.getKelurahan());
                                                tblSaksi.setDapildprkId(saksi.getDapildprkId());
                                                tblSaksi.setDapildpraId(saksi.getDapildpraId());
                                                tblSaksi.setDapildpriId(saksi.getDapildpriId());
                                                iss.insert(tblSaksi);
                                            } else {
                                                tblSaksi.setId(saksi.getSaksiId());
                                                tblSaksi.setImage(saksi.getImage());
                                                tblSaksi.setAddress(saksi.getAddress());
                                                tblSaksi.setPartyId(saksi.getNourutpartai());
                                                tblSaksi.setTpsId(saksi.getTpsId());
                                                tblSaksi.setTps(Integer.parseInt(saksi.getTps()));
                                                tblSaksi.setUsers(saksi.getUserId());
                                                tblSaksi.setName(saksi.getName());
                                                tblSaksi.setKecamatan(saksi.getKecamatan());
                                                tblSaksi.setKabupaten(saksi.getKabupaten());
                                                tblSaksi.setKelurahan(saksi.getKelurahan());
                                                tblSaksi.setDapildprkId(saksi.getDapildprkId());
                                                tblSaksi.setDapildpraId(saksi.getDapildpraId());
                                                tblSaksi.setDapildpriId(saksi.getDapildpriId());
                                                iss.update(tblSaksi);
                                            }
                                        }
                                    }
                                } else {
                                    getUser(notelpon);
                                }

                            } catch (ClassCastException cce) {
                                Log.e(TAG, "ClassCastException : " + cce.getMessage());
                            } catch (NumberFormatException nfe) {
                                Log.e(TAG, "NumberFormatException :" + nfe.getMessage());
                            }
                        } else {

                        }
                    }

                    @Override
                    public void onFailure(Call<CommonSialeg> call, Throwable t) {

                    }
                });
            }
        }

    }

    public void getPartai(){


        SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);


        Call<CommonSialeg> sialegCall = sourceData.getPartai();

        sialegCall.enqueue(new Callback<CommonSialeg>() {
            @Override
            public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {

                if (response.isSuccessful()) {

                    List<Partai> partaiList = new ArrayList<>();

                    try {
                        partaiList = response.body().toList(Partai.class);

                        if(iPartySrv.all().size()==0) {
                            for (Partai party : partaiList) {
                                com.bungker.com.pna.entities.Partai partai = new com.bungker.com.pna.entities.Partai();
                                partai.setNama(party.getNamapartai());
                                partai.setId(Integer.parseInt(party.getNourutpartai()));
                                String[] splitUrlname = party.getImage().split("/");
                                partai.setGambar(splitUrlname[1]);
                                MyHelpers.imageDownload(context, "http://123.108.97.200/api/" + party.getImage(), party.getImage());
                                iPartySrv.insert(partai);
                            }
                        }
                        else if(response.body().getTotal() > iPartySrv.all().size()){

                            for (Partai party : partaiList) {
                                com.bungker.com.pna.entities.Partai partaiExist = iPartySrv.findById(Integer.parseInt(party.getNourutpartai())) ;
                                if(partaiExist==null) {

                                    com.bungker.com.pna.entities.Partai partai = new com.bungker.com.pna.entities.Partai();
                                    partai.setNama(party.getNamapartai());
                                    partai.setId(Integer.parseInt(party.getNourutpartai()));
                                    String[] splitUrlname = party.getImage().split("/");
                                    partai.setGambar(splitUrlname[1]);
                                    MyHelpers.imageDownload(context, "http://123.108.97.200/api/" + party.getImage(), party.getImage());
                                    iPartySrv.insert(partai);
                                }
                                else {

                                    if("".equals(partaiExist.getGambar()) || partaiExist.getGambar()==null) {
                                        if(partaiExist.getGambar()==null){

                                        }
                                        else if("".equals(partaiExist.getGambar())){

                                        }
                                        String[] splitUrlname = party.getImage().split("/");
                                        partaiExist.setGambar(splitUrlname[1]);
                                        MyHelpers.imageDownload(context, "http://123.108.97.200/api/" + party.getImage(), party.getImage());
                                        iPartySrv.update(partaiExist);
                                    }
                                }
                            }
                        }
                        else {


                            for (Partai party : partaiList) {
                                com.bungker.com.pna.entities.Partai partaiExist = iPartySrv.findById(Integer.parseInt(party.getNourutpartai()));
                                if(partaiExist.getGambar()==null){

                                }
                                else if("".equals(partaiExist.getGambar())){

                                }
                                else {
                                    String[] splitName = party.getImage().split("/");
                                    File myDir = new File(Environment.getExternalStorageDirectory().getPath() + "/sialeg/" + splitName[0] + "/");
                                    myDir.mkdirs();
                                    //String fname = Calendar.getInstance().getTimeInMillis() + ".png";
                                    String fname = splitName[1];
                                    File file = new File (myDir, fname);
                                    if (!file.exists ()) {
                                        MyHelpers.imageDownload(context, "http://123.108.97.200/api/" + party.getImage(), party.getImage());
                                    }
                                }
                            }
                        }
                    } catch (ClassCastException cce) {
                        Log.e("ClassCastException : ", cce.getMessage());
                    }
                }
                else {
                    Log.i(TAG,"Response tidak success");
                }
            }

            @Override
            public void onFailure(Call<CommonSialeg> call, Throwable t) {
                Toast.makeText(context, "Tidak bisa menghubungi server !", Toast.LENGTH_SHORT).show();
            }
        });
    }

    public void getCaleg(int dprri, int dpra, int dprk){
        if(iPartySrv.all().size() > 0) {

            SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);

            Call<CommonSialeg> sialegCall = sourceData.getCaleg(dprri,dpra,dprk);

            sialegCall.enqueue(new Callback<CommonSialeg>() {
                @Override
                public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {

                    if (response.isSuccessful()) {

                        List<Caleg> calegList = new ArrayList<>();

                        try {
                            calegList = response.body().toList(Caleg.class);
                            if(iCalegSrv.all().size()==0) {
                                for (Caleg caleg : calegList) {
                                    com.bungker.com.pna.entities.Caleg calegEntity = new com.bungker.com.pna.entities.Caleg();

                                    calegEntity.setId_caleg(Integer.parseInt(caleg.getId()));
                                    calegEntity.setNama(caleg.getName());
                                    String[] dirNfile = caleg.getImage().split("/");
                                    calegEntity.setCalegImage(dirNfile[1]);
                                    MyHelpers.imageDownload(context, SialegConstant.myUrl + caleg.getImage(), caleg.getImage());
                                    calegEntity.setNoUrut(Integer.parseInt(caleg.getNumber()));
                                    calegEntity.setPartaId(Integer.parseInt(caleg.getPartyId()));
                                    calegEntity.setLevel(Integer.parseInt(caleg.getLevel()));
                                    calegEntity.setDapilDprri(caleg.getDapilDprri());
                                    calegEntity.setDapilDpra(caleg.getDapilDpra());
                                    calegEntity.setDapilDrpk(caleg.getDapilDrpk());
                                    iCalegSrv.insert(calegEntity);
                                }
                            }
                            else {


                                for (Caleg caleg : calegList) {
                                    com.bungker.com.pna.entities.Caleg calegCommon = iCalegSrv.findById(Integer.parseInt(caleg.getId()));
                                    if(calegCommon==null){
                                        calegCommon = new com.bungker.com.pna.entities.Caleg();

                                        calegCommon.setId_caleg(Integer.parseInt(caleg.getId()));
                                        calegCommon.setNama(caleg.getName());
                                        String[] dirNfile = caleg.getImage().split("/");
                                        calegCommon.setCalegImage(dirNfile[1]);
                                        MyHelpers.imageDownload(context, SialegConstant.myUrl + caleg.getImage(), caleg.getImage());
                                        calegCommon.setNoUrut(Integer.parseInt(caleg.getNumber()));
                                        calegCommon.setPartaId(Integer.parseInt(caleg.getPartyId()));
                                        calegCommon.setLevel(Integer.parseInt(caleg.getLevel()));
                                        calegCommon.setDapilDprri(caleg.getDapilDprri());
                                        calegCommon.setDapilDpra(caleg.getDapilDpra());
                                        calegCommon.setDapilDrpk(caleg.getDapilDrpk());
                                        iCalegSrv.insert(calegCommon);
                                    }
                                    else {
                                        if (calegCommon.getCalegImage() == null) {
                                            Log.i(TAG, "jika gambar caleg nulll");
                                        } else if ("".equals(calegCommon.getCalegImage())) {
                                            Log.i(TAG, "jika gambar caleg string kosong");
                                        } else {

                                            String[] splitName = caleg.getImage().split("/");
                                            File myDir = new File(Environment.getExternalStorageDirectory().getPath() + "/sialeg/" + splitName[0] + "/");
                                            myDir.mkdirs();
                                            //String fname = Calendar.getInstance().getTimeInMillis() + ".png";
                                            String fname = splitName[1];
                                            File file = new File(myDir, fname);
                                            if (!file.exists()) {
                                                MyHelpers.imageDownload(context,  SialegConstant.myUrl + caleg.getImage(), caleg.getImage());
                                            }
                                        }
                                    }
                                }
                            }
                        } catch (ClassCastException cce) {
                            Log.e("ClassCastException : ", cce.getMessage());
                        }
                        catch(NullPointerException npe){
                            Log.e(TAG, " npe : " + npe.getMessage());
                        }
                    }
                    else{

                    }
                }

                @Override
                public void onFailure(Call<CommonSialeg> call, Throwable t) {
                    Toast.makeText(context, "Tidak bisa mengontak server!",Toast.LENGTH_SHORT).show();
                }
            });
        }

    }

    public void getVoteCaleg(final int userid, List<com.bungker.com.pna.entities.Caleg> calegs){

        if(calegs.size() > 0 && iss.all().size() > 0) {

            for (com.bungker.com.pna.entities.Caleg caleg : calegs) {

                com.bungker.com.pna.entities.Saksi saksi = iss.findByUserId(userid);

                if (saksi != null) {
                    VoteCaleg vt = ivCalegSrv.findByTpsUserIdLevel(saksi.getTpsId(), userid, caleg.getLevel(), caleg.getId_caleg());
                    if(vt==null) {
                        VoteCaleg vcRi = new VoteCaleg();
                        vcRi.setUserId(userid);
                        vcRi.setPartai_id(caleg.getPartaId());
                        vcRi.setLevel(caleg.getLevel());
                        vcRi.setJlhSuara(0);
                        vcRi.setCalegId(caleg.getId_caleg());
                        vcRi.setTpsId(saksi.getTpsId());
                        ivCalegSrv.insert(vcRi);
                    }
                }
            }
        }
    }

    public void getVotePartai(List<com.bungker.com.pna.entities.Partai> partaiList, int userId){

        //Partai pkb  = IPartaiService.findById(1);
        User usr = IUserService.findById(userId);

        if(ivPartySrv.all().size()==0 ) {
            for (com.bungker.com.pna.entities.Partai partai : partaiList) {
                if (usr != null) {

                    VoteParty vpDprk = new VoteParty();
                    vpDprk.setLevel(3);
                    vpDprk.setPartyId(partai.getId());
                    vpDprk.setJlhSuara(0);
                    vpDprk.setTpsId(iss.findByUserId(userId).getTpsId());
                    vpDprk.setUserId(usr.getId());

                    VoteParty vpDprd = new VoteParty();
                    vpDprd.setLevel(2);
                    vpDprd.setPartyId(partai.getId());
                    vpDprd.setJlhSuara(0);
                    vpDprd.setTpsId(iss.findByUserId(userId).getTpsId());
                    vpDprd.setUserId(usr.getId());

                    VoteParty vpDprri = new VoteParty();
                    vpDprri.setLevel(1);
                    vpDprri.setPartyId(partai.getId());
                    vpDprri.setJlhSuara(0);
                    vpDprri.setTpsId(iss.findByUserId(userId).getTpsId());
                    vpDprri.setUserId(usr.getId());

                    IVotePartyService.insert(vpDprd, vpDprk, vpDprri);
                }
            }
        }
        else {
            for (com.bungker.com.pna.entities.Partai partai : partaiList) {
                List<VoteParty> vpList = ivPartySrv.findByPartyId(partai.getId());

                if (vpList.size() == 0) {
                    VoteParty vpDprk = new VoteParty();
                    vpDprk.setLevel(3);
                    vpDprk.setPartyId(partai.getId());
                    vpDprk.setJlhSuara(0);
                    vpDprk.setTpsId(iss.findByUserId(userId).getTpsId());
                    vpDprk.setUserId(usr.getId());
                    ivPartySrv.insert(vpDprk);

                    VoteParty vpDprd = new VoteParty();
                    vpDprd.setLevel(2);
                    vpDprd.setPartyId(partai.getId());
                    vpDprd.setJlhSuara(0);
                    vpDprd.setTpsId(iss.findByUserId(userId).getTpsId());
                    vpDprd.setUserId(usr.getId());
                    ivPartySrv.insert(vpDprd);

                    VoteParty vpDprri = new VoteParty();
                    vpDprri.setLevel(1);
                    vpDprri.setPartyId(partai.getId());
                    vpDprri.setJlhSuara(0);
                    vpDprri.setTpsId(iss.findByUserId(userId).getTpsId());
                    vpDprri.setUserId(usr.getId());
                    ivPartySrv.insert(vpDprri);

                    IVotePartyService.insert(vpDprd, vpDprk, vpDprri);
                } else {
                    for (VoteParty vp : vpList) {

//                        if (vp.getLevel() > 2) {
//
//                            VoteParty vpDprk = new VoteParty();
//                            vpDprk.setLevel(3);
//                            vpDprk.setPartyId(partai.getId());
//                            vpDprk.setJlhSuara(0);
//                            vpDprk.setTpsId(12);
//                            vpDprk.setUserId(usr.getId());
//                            ivPartySrv.insert(vpDprk);
//                        }
//                        else  if (vp.getLevel() == 2) {
//                            VoteParty vpDprd = new VoteParty();
//                            vpDprd.setLevel(2);
//                            vpDprd.setPartyId(partai.getId());
//                            vpDprd.setJlhSuara(0);
//                            vpDprd.setTpsId(12);
//                            vpDprd.setUserId(usr.getId());
//                            ivPartySrv.insert(vpDprd);
//                        }
//
//                        if (vp.getLevel() == 1) {
//                            VoteParty vpDprri = new VoteParty();
//                            vpDprri.setLevel(1);
//                            vpDprri.setPartyId(partai.getId());
//                            vpDprri.setJlhSuara(0);
//                            vpDprri.setTpsId(12);
//                            vpDprri.setUserId(usr.getId());
//                            ivPartySrv.insert(vpDprri);
//                        }
                    }
                }
            }

        }
    }

    public  Bitmap getBitmapFromURL(String src) {
        try {
            URL url = new URL(src);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            Toast.makeText(context, TAG + "Gagal convert ", Toast.LENGTH_SHORT);
            return null;
        }
    }

    private String saveFile(Bitmap sourceUri) {

        String root = Environment.getExternalStorageDirectory().toString();
        File myDir = new File(root + "/androidlift/partai/");
        myDir.mkdirs();
        String fname = Calendar.getInstance().getTimeInMillis() + ".png";
        File file = new File (myDir, fname);
        if (file.exists ()) file.delete ();
        try {
            if(sourceUri!=null) {
                FileOutputStream out = new FileOutputStream(file);
                sourceUri.compress(Bitmap.CompressFormat.PNG, 90, out);
                out.flush();
                out.close();
                return fname;
            }
            else {
                Toast.makeText(context , TAG + " Bitmap null ", Toast.LENGTH_SHORT).show();
                return "noname.png";
            }

        } catch (Exception e) {
            e.printStackTrace();
            Toast.makeText(context, TAG + " Gagal menyimpan", Toast.LENGTH_SHORT).show();
            return "noname.png";
        }
    }
}
