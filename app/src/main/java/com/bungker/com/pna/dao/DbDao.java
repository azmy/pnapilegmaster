package com.bungker.com.pna.dao;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;

import com.bungker.com.pna.entities.Caleg;
import com.bungker.com.pna.entities.FormC1;
import com.bungker.com.pna.entities.Group;
import com.bungker.com.pna.entities.Partai;
import com.bungker.com.pna.entities.Saksi;
import com.bungker.com.pna.entities.Setting;
import com.bungker.com.pna.entities.User;
import com.bungker.com.pna.entities.VoteCaleg;
import com.bungker.com.pna.entities.VoteParty;

@Database(entities = {User.class, Group.class, Partai.class, Caleg.class,
        VoteParty.class, VoteCaleg.class, Setting.class, Saksi.class, FormC1.class},
        version = 2, exportSchema = false)
public abstract class DbDao extends RoomDatabase {

    private static DbDao  INSTANCE;

    public abstract  UserDao userDao();
    public abstract  GroupDao groupDao();
    public abstract PartaiDao partaiDao();
    public abstract CalegDao calegDao();
    public abstract VoteCalegDao voteCalegDao();
    public abstract VotePartyDao votePartyDao();
    public abstract SettingDao settingDao();
    public abstract SaksiDao saksiDao();
    public abstract CheckVoteDao checkVoteDao();
    public abstract  FormC1Dao formC1Dao();



    public static DbDao getDatabase(final Context ctx) {
        if (INSTANCE == null) {
            synchronized (DbDao.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(ctx.getApplicationContext(),
                            DbDao.class, "caleg1_db")
                            .build();
                }
            }
        }
        return INSTANCE;
    }
}
