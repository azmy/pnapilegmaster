package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.Group;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface GroupDao {
    @Query("SELECT * FROM groups")
    List<Group> all();

    @Query("SELECT * FROM groups")
    Flowable<List<Group>> getAll();

    @Query("SELECT * from groups where id=:id")
    Group findOne(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(Group group);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(Group group);

    @Delete
    void delete(Group... groups);
}
