package com.bungker.com.pna.ui;


import android.arch.lifecycle.ViewModel;

import com.bungker.com.pna.entities.VotePartyWithParty;
import com.bungker.com.pna.services.VpwpDataSource;

import java.util.List;

import io.reactivex.Flowable;
import io.reactivex.functions.Function;

/**
 * View model untuk VotePartyWithParty
 */
public class VpwpViewModel extends ViewModel {

   private final VpwpDataSource vpwpDataSource;
   private List<VotePartyWithParty> vpwpList;

    public VpwpViewModel(VpwpDataSource vpwpDataSource) {
        this.vpwpDataSource = vpwpDataSource;
    }

    public Flowable<List<VotePartyWithParty>> getVotePartyByLevel(int level){
        return vpwpDataSource.findByLevel(level)
                .map(new Function<List<VotePartyWithParty>, List<VotePartyWithParty>>() {
                    @Override
                    public List<VotePartyWithParty> apply(List<VotePartyWithParty> votePartyWithParties) throws Exception {
                        return votePartyWithParties;
                    }
                });
    }
}
