package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.VoteParty;

import java.util.List;

@Dao
public interface VotePartyDao {

    @Insert
    long insert(VoteParty vp);

    @Update
    int  update(VoteParty vp);

    @Query("Select * from vote_partai")
    List<VoteParty> all();

    @Insert
    void insert(VoteParty... vcs);

    @Query("select * from vote_partai where id=:integer")
    VoteParty findOne(Integer integer);

    @Query("select * from vote_partai where party_id=:partyId")
    List<VoteParty> findByPartyId(int partyId);

    @Query("select * from vote_partai where tps_id=:tps and user_id=:user and party_id=:party and level=:level")
    VoteParty findByTpsUserPartyLevel(Integer tps, Integer user, Integer party, Integer level);
}
