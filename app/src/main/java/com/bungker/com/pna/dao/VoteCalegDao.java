package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.VoteCaleg;

import java.util.List;

@Dao
public interface VoteCalegDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insert(VoteCaleg vc);

    @Update
    int update(VoteCaleg vc);

    @Delete
    int delete(VoteCaleg vc);

    @Query("select * from vote_caleg")
    List<VoteCaleg> all();

    @Query("select * from vote_caleg where tps_id=:tps and user_id=:userid and  level=:level and caleg_id=:calegid")
    VoteCaleg findByTpsUserIdLevel(int  tps, int  userid, int level, int calegid);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insert(VoteCaleg... vcs);

    @Query("select  * from vote_caleg where id=:integer")
    VoteCaleg findOne(Integer integer);
}
