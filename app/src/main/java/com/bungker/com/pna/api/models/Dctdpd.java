package com.bungker.com.pna.api.models;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Dctdpd {

    @SerializedName("Dapil")
    @Expose
    private String dapil;
    @SerializedName("NoUrut")
    @Expose
    private Integer noUrut;
    @SerializedName("NamaLengkap")
    @Expose
    private String namaLengkap;
    @SerializedName("JenisKelamin")
    @Expose
    private String jenisKelamin;
    @SerializedName("TempatTinggal")
    @Expose
    private String tempatTinggal;
    @SerializedName("URL")
    @Expose
    private String uRL;

    public String getDapil() {
        return dapil;
    }

    public void setDapil(String dapil) {
        this.dapil = dapil;
    }

    public Integer getNoUrut() {
        return noUrut;
    }

    public void setNoUrut(Integer noUrut) {
        this.noUrut = noUrut;
    }

    public String getNamaLengkap() {
        return namaLengkap;
    }

    public void setNamaLengkap(String namaLengkap) {
        this.namaLengkap = namaLengkap;
    }

    public String getJenisKelamin() {
        return jenisKelamin;
    }

    public void setJenisKelamin(String jenisKelamin) {
        this.jenisKelamin = jenisKelamin;
    }

    public String getTempatTinggal() {
        return tempatTinggal;
    }

    public void setTempatTinggal(String tempatTinggal) {
        this.tempatTinggal = tempatTinggal;
    }

    public String getURL() {
        return uRL;
    }

    public void setURL(String uRL) {
        this.uRL = uRL;
    }

}
