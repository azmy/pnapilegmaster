package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import com.bungker.com.pna.entities.Saksi;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface SaksiDao {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    long insert(Saksi saksi);

    @Update(onConflict = OnConflictStrategy.REPLACE)
    int update(Saksi saksi);

    @Delete
    int delete(Saksi saksi);

    @Query("SELECT * from witnesses")
    List<Saksi> all();

    @Query("SELECT * from witnesses")
    Flowable<List<Saksi>> getall();

    @Query("SELECT * from witnesses where user_id=:userid limit 1")
    Flowable<Saksi> findByUserId(Integer userid);

    @Query("SELECT *  from witnesses where id=:id")
    Saksi findById(int id);

    @Query("SELECT * from witnesses where user_id=:userid limit 1")
    Saksi getByUserId(Integer userid);


}
