package com.bungker.com.pna.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;


@Entity(tableName = "image_form_c1")
public class FormC1 {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "tps_id")
    private int tps;

    @ColumnInfo(name = "user_id")
    private int userid;

    @ColumnInfo(name = "level")
    private int level = 0;

    @ColumnInfo(name="path")
    private String imagePath;

    @ColumnInfo(name="is_sent")
    private Integer isSent = 0;

    public Integer getIsSent() {
        return isSent;
    }

    public void setIsSent(Integer isSent) {
        this.isSent = isSent;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    public int getUserid() {
        return userid;
    }
    public void setUserid(int userid) {
        this.userid = userid;
    }

    public int getTps() {
        return tps;
    }

    public void setTps(int tps) {
        this.tps = tps;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

}
