package com.bungker.com.pna.tools;

public interface DataSource<T> {
    T getDataSource();
}
