package com.bungker.com.pna.api.services;

import com.bungker.com.pna.api.models.CommonModel;

import retrofit2.Call;
import retrofit2.http.GET;

public interface PemiluSrv {

    @GET("/dprdkabkota")
    public Call<CommonModel> getData();

    @GET("/dprdprovinsi")
    public Call<CommonModel> getProvLevel();

    @GET("/dprri")
    public Call<CommonModel> dprri();
}
