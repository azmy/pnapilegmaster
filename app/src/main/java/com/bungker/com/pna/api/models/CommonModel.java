package com.bungker.com.pna.api.models;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

public class CommonModel {

    @SerializedName("count")
    @Expose
    private Integer count;


    @SerializedName("data")
    @Expose
    private List<Object> data;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public List<Object> getDatas() {
        return data;
    }

    public void setDatas(List<Object> datas) {
        this.data = datas;
    }

    public <T> List<T> toList( Class<T> desiredClass) {
        List<T> transformedList = new ArrayList<>();
        if (getDatas() != null) {
            for (Object result : getDatas()) {
                String json = new Gson().toJson(result);
                T model = new Gson().fromJson(json, desiredClass);
                transformedList.add(model);
            }
        }
        return transformedList;
    }
}