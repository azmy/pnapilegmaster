package com.bungker.com.pna.entities;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

@Entity(tableName = "vote_partai")
public class VoteParty {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @ColumnInfo(name = "tps_id")
    private int tpsId;

    @ColumnInfo(name = "jumlah_suara")
    private int jlhSuara=0;

    @ColumnInfo(name = "party_id")
    private int partyId;

    @ColumnInfo(name = "user_id")
    private int userId;

    @ColumnInfo(name="level")
    int level;

    @ColumnInfo(name="is_sent")
    int isSent=0;

    public int getIsSent() {
        return isSent;
    }

    public void setIsSent(int isSent) {
        this.isSent = isSent;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getTpsId() {
        return tpsId;
    }

    public void setTpsId(int tpsId) {
        this.tpsId = tpsId;
    }

    public int getJlhSuara() {
        return jlhSuara;
    }

    public void setJlhSuara(int jlhSuara) {
        this.jlhSuara = jlhSuara;
    }

    public int getPartyId() {
        return partyId;
    }

    public void setPartyId(int partyId) {
        this.partyId = partyId;
    }

    public int getUserId() {
        return userId;
    }

    public void setUserId(int userId) {
        this.userId = userId;
    }
}
