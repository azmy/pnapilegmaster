package com.bungker.com.pna.api.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Caleg {

    @SerializedName("id")
    @Expose
    private String id;
    @SerializedName("name")
    @Expose
    private String name;

    @SerializedName("sex")
    @Expose
    private String sex;

    @SerializedName("number")
    @Expose
    private String number;

    @SerializedName("image")
    @Expose
    private String image;

    @SerializedName("party_id")
    @Expose
    private String partyId;

    @SerializedName("level")
    @Expose
    private String level;

    @SerializedName("dapildpri_id")
    @Expose
    private int dapilDrpk;

    @SerializedName("dapildpra_id")
    @Expose
    private int  dapilDpra;

    @SerializedName("dapildprk_id")
    @Expose
    private int  dapilDprri;

    public int getDapilDrpk() {
        return dapilDrpk;
    }

    public void setDapilDrpk(int dapilDrpk) {
        this.dapilDrpk = dapilDrpk;
    }

    public int getDapilDpra() {
        return dapilDpra;
    }

    public void setDapilDpra(int dapilDpra) {
        this.dapilDpra = dapilDpra;
    }

    public int getDapilDprri() {
        return dapilDprri;
    }

    public void setDapilDprri(int dapilDprri) {
        this.dapilDprri = dapilDprri;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getPartyId() {
        return partyId;
    }

    public void setPartyId(String partyId) {
        this.partyId = partyId;
    }

    public String getLevel() {
        return level;
    }

    public void setLevel(String level) {
        this.level = level;
    }
}
