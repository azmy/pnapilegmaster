package com.bungker.com.pna.dao;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.bungker.com.pna.entities.FormC1;

import java.util.List;

import io.reactivex.Flowable;

@Dao
public interface FormC1Dao {

    @Query("SELECT * FROM image_form_c1 where user_id=:user_id and level=:level")
    Flowable<List<FormC1>> all(int user_id, int level);

    @Query("SELECT * from image_form_c1 where id=:id")
    Flowable<FormC1> findOne(int id);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    long insertAndUpdate(FormC1 c1);

    @Delete
    void delete(FormC1... formC1s);

    @Query("DELETE FROM image_form_c1 WHERE id = :id")
    int deleteBypath(int id);

    @Query("UPDATE image_form_c1 SET is_sent='1' WHERE id = :id and is_sent='0'")
    int update_Is_Sent_By_ID(int id);

    @Query("Select * from image_form_c1  WHERE user_id = :id")
    int findByUser(int id);


}
