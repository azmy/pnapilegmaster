package com.bungker.com.pna.tools;

import android.content.Context;

import com.bungker.com.pna.dao.DbDao;
import com.bungker.com.pna.services.FormC1DataSource;
import com.bungker.com.pna.services.SaksiDataSource;
import com.bungker.com.pna.services.VcwcDatasource;
import com.bungker.com.pna.services.VpwpDataSource;
import com.bungker.com.pna.services.implement.IFormC1DataSource;
import com.bungker.com.pna.services.implement.ISaksiDataSource;
import com.bungker.com.pna.services.implement.IVcwcDatasource;
import com.bungker.com.pna.services.implement.IVpwpDataSource;


public class Injection {

    public static SaksiDataSource providerSaksiDataSource(Context context){
        DbDao db =DbDao.getDatabase(context);
        return new ISaksiDataSource(db.saksiDao());
    }

    public static FormC1DataSource providerFormC1DataSource(Context context){
        DbDao db =DbDao.getDatabase(context);
        return  new IFormC1DataSource(db.formC1Dao());
    }


    public static VcwcDatasource providerVcwcDataSource(Context context){
        DbDao db = DbDao.getDatabase(context);
        return new IVcwcDatasource(db.calegDao());
    }

    public static VpwpDataSource providerVpwpDataSource(Context context){
        DbDao db = DbDao.getDatabase(context);
        return new IVpwpDataSource(db.partaiDao());
    }


    public static ViewModelFactory provideViewModelFactory(Context context,  String type){
        ViewModelFactory vmf = new ViewModelFactory(type);
        if(type.equals("saksiDataSource")) {
            SaksiDataSource dataSource = providerSaksiDataSource(context);
            vmf.setSaksiDataSource(dataSource);
        }
        else if(type.equals("formC1DataSource")){
            FormC1DataSource  dataSource = providerFormC1DataSource(context);
            vmf.setFormC1DataSource(dataSource);
        }

        else if(type.equals("vcwcDataSource")) {
            VcwcDatasource datasource = providerVcwcDataSource(context);
            vmf.setVcwcDatasource(datasource);
        }
        else if(type.equals("vpwpDataSource")){
            VpwpDataSource dataSource = providerVpwpDataSource(context);
            vmf.setVpwpDataSource(dataSource);
        }

        //return new ViewModelFactory(dataSource, type);
        return  vmf;
    }


}
