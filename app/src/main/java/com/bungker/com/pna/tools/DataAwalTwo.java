package com.bungker.com.pna.tools;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.bungker.com.pna.api.models.CommonSialeg;
import com.bungker.com.pna.api.models.Group;
import com.bungker.com.pna.api.services.SialegSrv;
import com.bungker.com.pna.api.tools.ApiSiAleg;
import com.bungker.com.pna.dao.DbDao;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class DataAwalTwo extends AsyncTask<Void,Void,Void> {

    private static final String TAG="DataAwalTwo";
    private Context context;
    private DbDao db;

    public DataAwalTwo(Context context){
        this.context = context;
        db = DbDao.getDatabase(context);
    }

    @Override
    protected Void doInBackground(Void... voids) {
        System.out.println(TAG + " doInBackground");
        if(db.groupDao().all().size() ==0){

            SialegSrv sourceData = ApiSiAleg.newIntance().createService(SialegSrv.class);

            Call<CommonSialeg> sialegCall = sourceData.getGroup();

            sialegCall.enqueue(new Callback<CommonSialeg>() {
                @Override
                public void onResponse(Call<CommonSialeg> call, Response<CommonSialeg> response) {
                    if (response.isSuccessful()) {

                        List<Group> groupList = new ArrayList<>();

                        try {
                            groupList = response.body().toList(com.bungker.com.pna.api.models.Group.class);
                            for (com.bungker.com.pna.api.models.Group group : groupList) {
                                com.bungker.com.pna.entities.Group gp = new com.bungker.com.pna.entities.Group();
                                gp.setName(group.getName());
                                gp.setId(Integer.parseInt(group.getId()));
                                db.groupDao().insert(gp);
                            }
                        } catch (ClassCastException cce) {
                            Log.e("ClassCastException : ", cce.getMessage());
                        }
                    }
                }

                @Override
                public void onFailure(Call<CommonSialeg> call, Throwable t) {

                }
            });
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        System.out.println(TAG + " onPreExecute");
    }

    @Override
    protected void onCancelled() {
        super.onCancelled();
        System.out.println(TAG + " onCancelled");
    }

    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        System.out.println(TAG + " onPostExecute");
    }
}
